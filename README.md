
## Měření
Měření proběhlo na 4 jádrovém procesoru intel CORE i5

# matrix_multiplicator

matrix_multiplicator je program pro násobení matic.
Kromě jednoduché funkcionality ale ukazuje také rozdíly mezi
jednotlivými přístupy k násobení matic v implementaci v jazyce c++.
První nejjednoduší je způsob sériový. Druhý a třetí jsou jen pro
porovnání se čtvrtým a zde je využita knihovna omp, která umožňuje
snažší přístup k vláknům, zato nám ale nepovoluje takový rozsah.
Čtvrtý způsob používá klasická vlákna.

## Implementace
Implementace pro jedno vlákno je triviální, vícevláknové implementace
pak mají sdílené proměnné do kterých zapisují a ze kterých čtou
a každé vlákno si zde bere na starosti jiný z vektorů.

Matice se dají zadat na standardní vstup nebo poslat klasicky na vstup
ze souboru pomocí operátoru "<" na příkazové řádce. V kódu se dájí také
odkomentovat řádky, které vytvoří matice o zadaných velikostech
s náhodnými prvky.

## Valgrind
Program jsem několikrát testoval a upravoval s vagrindem, než jsem zjistil,
že knihovna omp zanechává nějaké memory leaks a je to dle více zdroju
(např.: https://medium.com/@auraham/pseudo-memory-leaks-when-using-openmp-11a383cc4cf9)
naprosto v pořádku. Dlouho jsem se snažil neuvolněné bloky uvolnit, než jsem
narazil na výše zmíněný článek.

## Měření
Měření proběhlo na 4 jádrovém procesoru intel CORE i5
CPU taktovaném na 3.5 GHz.

Výstup pro různé hodnoty měření:
```
1000 x 1000 matrix
1000 x 1000 matrix
Serial ----------- 9.243444s
Parallel (1)------ 4.781469s correct
Parallel (2)------ 4.854810s correct
Threads  (3)------ 4.780999s correct
10000 x 1000 matrix
1000 x 10000 matrix
Serial ----------- 1088.703995s
Parallel (1)------ 700.742327s correct
Parallel (2)------ 611.687812s correct
Threads  (3)------ 706.977294s correct
5000 x 5000 matrix
5000 x 5000 matrix
Serial ----------- 1647.662401s
Parallel (1)------ 867.943902s correct
Parallel (2)------ 867.169227s correct
Threads  (3)------ 856.691598s correct
```
