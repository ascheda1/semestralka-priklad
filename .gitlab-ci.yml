image: mjerabek/pjc_sandbox
before_script:
    # If your app needs some libraries, install them here (or use custom docker
    # image). For packages included in pjc_sandbox, look it up at Docker Hub.
    # - apt-get update && apt-get install -y -qq MY_LIBRARY...

    # sanitizer runtime options (passed as environment variables)
    - export UBSAN_OPTIONS=print_stacktrace=1:silence_unsigned_overflow=1:color=always
    - export ASAN_OPTIONS=strip_path_prefix=$PWD:replace_str=1:replace_intrin=1:detect_invalid_pointer_pairs=2:detect_container_overflow=1:strict_string_checks=1:detect_stack_use_after_return=1:check_initialization_order=1:strict_init_order=1:check_printf=1:color=always
    - export MSAN_OPTIONS=strip_path_prefix=$PWD:color=always
    - ln -s llvm-symbolizer-3.8 /usr/bin/llvm-symbolizer
    - CMAKE_COMMON_FLAGS=(-DCMAKE_BUILD_TYPE=Debug -DCMAKE_CXX_COMPILER=clang++-3.8 -GNinja -DCMAKE_CXX_FLAGS_DEBUG="-g -fdiagnostics-color" -DCMAKE_CXX_FLAGS_RELWITHDEBINFO="-fdiagnostics-color -g -O2 -DNDEBUG")

stages:
    - build
    - test
    - test_sanitizers
    - measure

build:
    stage: build
    script:
        # Release build
        - cmake "${CMAKE_COMMON_FLAGS[@]}" -DCMAKE_BUILD_TYPE="RelWithDebInfo" -Bbuild/release -H. && cmake --build build/release

        # Build with AddressSanitizer and UndefinedBehaviorSanitizer
        - cmake "${CMAKE_COMMON_FLAGS[@]}" -DCMAKE_CXX_FLAGS="-fsanitize=address -fsanitize=undefined" -Bbuild/ausan -H. && cmake --build build/ausan

        # Build with ThreadSanitizer
        - cmake "${CMAKE_COMMON_FLAGS[@]}" -DCMAKE_CXX_FLAGS="-fsanitize=thread" -Bbuild/tsan -H. && cmake --build build/tsan

        # Build with MemorySanitizer. This is tricky and requires that
        # all libraries (including C++ standard library) are built with it.
        # We supply instrumented libc++ and libfmt in the pjc_sandbox image, but not other libs.
        # If you use other libs, disable msan build & run, as it might give false positives
        # (or compile them yourself).
        - cmake "${CMAKE_COMMON_FLAGS[@]}" -DCMAKE_CXX_FLAGS="${msan_cxx_flags}"
                -DCMAKE_EXE_LINKER_FLAGS="${msan_exe_linker_flags}"
                -Bbuild/msan -H. && cmake --build build/msan

    artifacts:
        paths:
            - build/*/${app}
    # Cache the build products, so that we do not have to rebuild everything
    # from scratch. If you think something goes weird during the build, try
    # clearing runner cache (button on GitLab pipelines page).
    cache:
        paths:
            - build

# First, run some quick correctness tests - if it fails, no other tests are run.
# You may run the app with some simple input and compare if the output is corrent.
# Or even run some Catch unit tests, if you want to be thorough.
# The test jobs fails if some command fails (i.e. exits with non-zero error code)..
# NOTE: If you are looking for an error, the sanitizers may help you. Preferably run them
#       on your PC, but you may also set "allow_failure: yes" to not skip next tests.
test:
    stage: test
    # allow_failure: yes
    script:
        - build/release/${app} 10000
        # todo: check output correctness (e.g. by running
        #       `diff produced-file expected-file`)

# Now test the app with sanitizers
test_ausan:
    stage: test_sanitizers
    script:
        - build/ausan/${app} 10000

test_msan:
    stage: test_sanitizers
    script:
        - build/msan/${app} 10000

test_tsan:
    stage: test_sanitizers
    script:
        - build/tsan/${app} 10000

# if you run ausan+msan, running valgrind is redundant
test_valgrind:
    stage: test_sanitizers
    script:
        # run with less iterations to take less time
        - valgrind --leak-check=full build/release/${app} 10000

# running both helgrind and tsan is redundant, you may choose which one you like better
test_helgrind:
    stage: test_sanitizers
    script:
        # run with less iterations to take less time
        - valgrind --tool=helgrind build/release/${app} 10000

# run on real-sized input, generate nice outputs, ...
# NOTE: if you want to publish some generated files, include them in the "artifacts" section
#       (see the build job for an example)
run:
    stage: measure
    script:
        - ./build/release/${app}

# some common variables
variables:
    app: pi-count
    msan_cxx_flags: -fsanitize=memory -fsanitize-memory-track-origins -fno-omit-frame-pointer
                    -I/sysroot/msan/include -I/sysroot/msan/include/c++/v1 -stdlib=libc++
    msan_exe_linker_flags: -stdlib=libc++ -lc++abi -L/sysroot/msan/lib -Wl,-rpath,/sysroot/msan/lib
